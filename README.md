# flat-file-tests

A small helper for creating test suites for which the inputs/expected outputs are defined in flat files.

For example, testing a function that sorts a newline-separated list of words using AVA:

`test.js:`
```javascript
import test          from "ava";
import flatFileTests from "flat-file-tests";

let tests = flatFileTests.iterate(__dirname + "/assets", [ "input", "output" ]);
for(let testCase of tests) {
    test(testCase.getName(), async (t) => {
        let input = await testCase.get("input");
        let actual = sortFile(input);
        t.is(actual, await testCase.get("output"));
    });
}

function sortFile(contents) {
    return contents.split("\n").sort().join("\n");
}
```

`assets/test1/input.txt:`
```
skip
flaky
special
fuel
cent
broad
slippery
previous
transport
tame
hate
pin
```

`assets/test1/output.txt:`
```
broad
cent
flaky
fuel
hate
pin
previous
skip
slippery
special
tame
transport
```

`assets/test2/input.txt:`
```
wrong
order
oops
```

`assets/test2/output.txt:`
```
oops
wrong
order
```

![](media/ava-result.png)
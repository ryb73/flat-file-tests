"use strict";

const   ava           = require("ava"),
        _             = require("lodash"),
        flatFileTests = require("..");

testSuite1();
testSuite2();
testSuite3();

function testSuite1() {
    let suite1 = flatFileTests.iterate(__dirname + "/assets/suite1", [ "input", "output" ]);
    ava.test(t => {
        t.deepEqual(
            _.invokeMap(suite1, "getName"),
            [ "test1", "test2", "test3" ]
        );
    });

    for(let test of suite1) {
        ava.test(test.getName(), async (t) => {
            let input = await test.get("input");
            let expected = input.split("\n").sort().join("\n");
            t.is(await test.get("output"), expected);
        });
    }
}

function testSuite2() {
    let suite2 = flatFileTests.iterate(__dirname + "/assets/suite2", [ "a", "b", "c" ]);
    ava.test(t => {
        t.deepEqual(
            _.invokeMap(suite2, "getName"),
            [ "test1" ]
        );
    });

    for(let test of suite2) {
        ava.test(test.getName(), async (t) => {
            let a = +(await test.get("a"));
            let b = +(await test.get("b"));
            t.is(+(await test.get("c")), a + b);
        });
    }
}

function testSuite3() {
    let suite3 = flatFileTests.iterate(__dirname + "/assets/suite1", [ "a", "b", "c" ]);
    ava.test(t => t.deepEqual(suite3, []));
}

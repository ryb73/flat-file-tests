"use strict";

const   glob = require("glob"),
        _    = require("lodash"),
        fs   = require("fs"),
        path = require("path");

module.exports = {
    iterate(dir, assetNames) {
        let allDirs = glob.sync(dir + "/**/");
        let testDirs = allDirs.filter(hasTest.bind(null, assetNames));
        return testDirs.map(createTest);
    }
};

function hasTest(assetNames, directory) {
    return _.every(assetNames, (assetName) => {
        return fs.existsSync(directory + assetName);
    });
}

function createTest(directory) {
    return {
        getName() {
            return path.basename(directory);
        },

        get: async function(assetName) {
            let p = new Promise((resolve, reject) => {
                fs.readFile(directory + assetName, "utf8", (err, data) => {
                    if(err)
                        reject(err);
                    else
                        resolve(data);
                });
            });

            return p;
        }
    };
}